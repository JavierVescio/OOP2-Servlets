<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="datos.Cliente" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sistema Franc�s</title>
</head>
	<body>
		<!-- El tag con porcentajes permite embeber codigo java en la web 
		Es mas comodo embeber java en un html como aca que al reves.-->
		<%@ include file="/cabecera.jsp" %>
		<% Cliente cliente = (Cliente)request.getAttribute("cliente");%>
		<br>
			Apellido: <%=cliente.getApellido()%><br>
			Nombre: <%=cliente.getNombre()%><br>
			DNI: <%=cliente.getDni() %><br>
			ID: <%=cliente.getIdCliente() %><br>
		<br>
		<a href="/SistemaFrances/index.jsp">Volver...</a>
	</body>
</html>