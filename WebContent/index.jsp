<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Sistema Franc�s - Cliente</title>
	</head>
	<body>
		<%@ include file="/cabecera.jsp" %>
			<form method="POST" action="/SistemaFrances/MostrarClienteJSP">
				B�squeda de clientes<br><br>
				<table border="0">
					<tr>
						<td>DNI:</td>
						<td><input name="dni"></td>
					</tr>
					<tr>
						<input type="submit" value="Consultar">
					</tr>
				</table>
			</form>
		<br><br><br><br>
		<form method="POST" action="/SistemaFrances/ListarClientes">
			<input type="hidden" name="jstl" value="0"/>
			<table border="0">
				<tr>
					<td>
						<input type="submit" value="Listar Clientes">
					</td>
				</tr>
			</table>	
		</form>
		<br><br><br><br>
		<form method="POST" action="/SistemaFrances/ListarClientes">
			<input type="hidden" name="jstl" value="1"/>
			<table border="0">
				<tr>
					<td>
						<input type="submit" value="Listar Clientes con JSTL">
					</td>
				</tr>
			</table>
		</form>
		<br><br><br><br>
	</body>
</html>