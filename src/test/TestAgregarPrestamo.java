package test;

import java.util.GregorianCalendar;

import negocio.*;
import datos.*;

public class TestAgregarPrestamo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PrestamoABM prestamoABM = new PrestamoABM();
		ClienteABM clienteABM = new ClienteABM();

		try {
			long idCliente = 1;
			Cliente cliente = clienteABM.traerCliente(idCliente);
			System.out.println("Cliente: "+cliente);
			prestamoABM.agregar(new GregorianCalendar(2016, 10, 15), 6000, 12, 8, cliente);
		}catch(Exception e){
			System.out.println("Error: "+e.getMessage());
		}
	}
}
