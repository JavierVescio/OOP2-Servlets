package test;

import java.util.List;

import datos.Cliente;
import datos.Prestamo;
import negocio.ClienteABM;
import negocio.PrestamoABM;

public class TestTraerPrestamo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PrestamoABM prestamoABM = new PrestamoABM();
		long idPrestamo = 1;
		System.out.println("\n---> TraerPrestamo idPrestamo="+idPrestamo+"\n\n");
		try {
			Prestamo p = prestamoABM.traerPrestamo(idPrestamo);
			System.out.println(p + "\nPertenece a "+p.getCliente());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}


		ClienteABM clienteABM = new ClienteABM();
		int dni = 30545765;
		try {
			Cliente c = clienteABM.traerCliente(dni);

			System.out.println("\n---> Traer prestamos del cliente="+c+"\n\n");
			List<Prestamo> prestamos = prestamoABM.traerPrestamo(c);
			//Si este cliente no tiene prestamos mostrar mensaje

			for(Prestamo o:prestamos)
				System.out.println(o + "\n Pertenece a "+o.getCliente());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}


	}
}
