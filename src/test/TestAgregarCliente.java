package test;

import java.util.GregorianCalendar;

import negocio.ClienteABM;

public class TestAgregarCliente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String apellido= "Cvitanich";
		String nombre= "Dario";
		int documento=30545765;
		GregorianCalendar fechaDeNacimiento = new GregorianCalendar(1983,2,14); // tu fecha de nacimiento

		ClienteABM abm= new ClienteABM();

		long ultimoIdCliente;
		try {
			ultimoIdCliente = abm.agregar(apellido, nombre, documento, fechaDeNacimiento);
			System.out.println("El id cliente agregado es: "+ultimoIdCliente);
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}		
	}
}
